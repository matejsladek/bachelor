import React from 'react';
import Visualizer from './components/visualizer/index.jsx';

import './style.css';

class App extends React.Component {
    render() {
        return (
            <Visualizer />
        );
    }
}
export default App;
