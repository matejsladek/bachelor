import React from 'react';
import * as THREE from 'three';

import './style.css';

class AxesHelper extends React.Component{
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        this.cameraDistance = 300;
        const width = this.mount.clientWidth;
        const height = this.mount.clientHeight;
        console.log("asd", width, height);
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setClearColor( 0xf0f0f0, 1 );
        // this.renderer.setClearColor( 0xffffff, 1 );
        this.renderer.setSize( width, height );
        this.mount.appendChild(this.renderer.domElement);

        // scene
        this.scene = new THREE.Scene();

        // camera
        this.camera = new THREE.PerspectiveCamera( 50, width / height, 1, 1000 );
        // this.camera.position.z = 1;
        this.camera.up = this.props.mainCamera.up; // important!

        // axes
        this.axes = new THREE.AxesHelper( 100 );
        this.scene.add( this.axes );

        //text
        const loader = new THREE.FontLoader();

        // loader.load( 'fonts/helvetiker_regular.typeface.json', function ( font ) {
        const setAxesText = (function( font ) {
            const textSettings = {
                size: 8,
                height: 4,
                curveSegments: 6,
                font: font,
                style: "normal"
            };
            const textGeoX = new THREE.TextGeometry('X', textSettings);
            const textGeoY = new THREE.TextGeometry('Y', textSettings);
            const textGeoZ = new THREE.TextGeometry('Z', textSettings);

            const color = new THREE.Color();
            // color.setRGB(255, 250, 250);
            color.setRGB(255, 0, 0);
            const textMaterial = new THREE.MeshBasicMaterial({ color: color });
            this.textX = new THREE.Mesh(textGeoX , textMaterial);
            this.textY = new THREE.Mesh(textGeoY , textMaterial);
            this.textZ = new THREE.Mesh(textGeoZ , textMaterial);

            this.textX.position.x = this.axes.geometry.attributes.position.getX(1);
            this.textX.position.y = this.axes.geometry.attributes.position.getY(1);
            this.textX.position.z = this.axes.geometry.attributes.position.getZ(1);

            this.textY.position.x = this.axes.geometry.attributes.position.getX(3);
            this.textY.position.y = this.axes.geometry.attributes.position.getY(3);
            this.textY.position.z = this.axes.geometry.attributes.position.getZ(3);

            this.textZ.position.x = this.axes.geometry.attributes.position.getX(5);
            this.textZ.position.y = this.axes.geometry.attributes.position.getY(5);
            this.textZ.position.z = this.axes.geometry.attributes.position.getZ(5);
            // textZ.setRotationFromEuler(this.camera.rotation);
            // console.log('rotation')
            this.scene.add(this.textX);
            this.scene.add(this.textY);
            this.scene.add(this.textZ);
        }).bind(this);
        loader.load(
            'https://raw.githubusercontent.com/mrdoob/three.js/master/examples/fonts/helvetiker_regular.typeface.json',
            setAxesText
        );

        this.start();
    }
    componentWillUnmount(){
        this.stop();
        this.mount.removeChild(this.renderer.domElement);
    };
    start = () => {
        if (!this.frameId) {
            this.frameId = requestAnimationFrame(this.animate)
        }
    };
    stop = () => {
        cancelAnimationFrame(this.frameId)
    };
    animate = () => {
        this.camera.position.copy( this.props.mainCamera.position );
        // this.camera.position.sub( controls.center ); // added by @libe // neviem zatial https://stackoverflow.com/questions/16226693/three-js-show-world-coordinate-axes-in-corner-of-scene
        this.camera.position.setLength( this.cameraDistance );
        this.camera.lookAt( this.scene.position );
        if(this.textX != null){
            this.textX.setRotationFromEuler(this.camera.rotation);
            this.textY.setRotationFromEuler(this.camera.rotation);
            this.textZ.setRotationFromEuler(this.camera.rotation);
        }
        this.renderScene();
        this.frameId = window.requestAnimationFrame(this.animate)
    };
    renderScene = () => {
        this.renderer.render( this.scene, this.camera );
    };
    render(){
        return(
            <div
                className={"axes_helper"}
                ref={(mount) => { this.mount = mount }}
            />
        )
    }
}
export default AxesHelper;
