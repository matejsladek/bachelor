import React from 'react';
import MainScene from "../main_scene/index.jsx";
import AxesHelper from "../axes_helper/index.jsx";
import CameraImage from "../camera_image/index.jsx";
import * as THREE from "three";
import * as dat from 'dat.gui';

import './style.css'

class Visualizer extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showAxesHelper: true,
            showPointCloud: true,
            computeFeaturePoints: false,
            pointCloud: [],
            hashCodes: [],
        };
        // this.mainCamera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
        this.mainCamera = new THREE.PerspectiveCamera();
        this.onSetPointCloud = this.onSetPointCloud.bind(this);
        this.onSetHashCodes = this.onSetHashCodes.bind(this);
    }

    componentDidMount() {
        const params = {
            showPointCloud: true,
            showAxesHelper: true,
            computeFeaturePoints: false,
        };
        const gui = new dat.GUI();
        gui.domElement.id = 'gui';
        const controllerPointCloud = gui.add(params, 'showPointCloud');
        const controllerAxesHelper = gui.add(params, 'showAxesHelper');
        const controllerComputeFeaturePoints = gui.add(params, 'computeFeaturePoints');
        controllerPointCloud.onFinishChange((value) => {
            this.setState({showPointCloud: value});
        });
        controllerAxesHelper.onFinishChange((value) => {
            this.setState({showAxesHelper: value});
        });
        controllerComputeFeaturePoints.onFinishChange((value) => {
            this.setState({computeFeaturePoints: value});
        });
    }

    onSetPointCloud(pointCloud) {
        this.setState({pointCloud});
    }

    onSetHashCodes(hashCodes) {
        console.log('onsethashcodes', hashCodes);
        this.setState({hashCodes});
    }

    render(){
        return(
            <div>
                <MainScene
                    camera={this.mainCamera}
                    showPointCloud={this.state.showPointCloud}
                    hashCodes={this.state.hashCodes}
                    onSetPointCloud={this.onSetPointCloud}
                    onSetHashCodes={this.onSetHashCodes}
                />
                {this.state.showAxesHelper && <AxesHelper mainCamera={this.mainCamera}/>}
                <CameraImage
                    pointCloud={this.state.pointCloud}
                    computeFeaturePoints={this.state.computeFeaturePoints}
                    camera={this.mainCamera}
                />
                <div className={'devicesInfo'}>
                    { this.state.hashCodes.map((hashCode, i) => {
                        return <p> hashCode: {hashCode} </p>
                    })}
                </div>
            </div>
        )
    }
}
export default Visualizer;
